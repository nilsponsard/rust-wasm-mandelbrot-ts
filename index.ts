import { MandelbrotViewport } from "wasm-mandelbrot";
import { memory } from "wasm-mandelbrot/wasm_mandelbrot_bg";


const precision_slider = <HTMLInputElement>document.getElementById("max_i_slider")
const high_res_button = <HTMLButtonElement>document.getElementById("high_res_button")
const res_width_input = <HTMLButtonElement>document.getElementById("res_width")
const input_x = <HTMLButtonElement>document.getElementById("input_x")
const input_y = <HTMLButtonElement>document.getElementById("input_y")
const zoom_in_button = <HTMLInputElement>document.getElementById("zoom_in")
const zoom_out_button = <HTMLInputElement>document.getElementById("zoom_out")

const perf_moy_span = <HTMLSpanElement>document.getElementById("perf_moy")


const canvas = <HTMLCanvasElement>document.getElementById("canvas")
const ctx = <CanvasRenderingContext2D>canvas.getContext("2d")
// const WIDTH = window.innerWidth
// const HEIGHT = window.innerHeight
const WIDTH = 400
const HEIGHT = 400
canvas.width = WIDTH
canvas.height = HEIGHT
ctx.imageSmoothingEnabled = false


high_res_button.addEventListener("click", event => {
    let save_canvas = <HTMLCanvasElement>document.createElement("canvas")
    let save_canvas_ctx = <CanvasRenderingContext2D>save_canvas.getContext("2d")
    save_canvas.width = parseInt(res_width_input.value, 10)
    save_canvas.height = parseInt(res_width_input.value, 10)
    let cloned_viewport = MandelbrotViewport.new(viewport.get_width(), viewport.get_height(), viewport.get_x(), viewport.get_y(), save_canvas.width, save_canvas.height)
    cloned_viewport.set_max_i(viewport.get_max_i())
    cloned_viewport.draw()
    let data_high_res = new ImageData(get_data(cloned_viewport), save_canvas.width, save_canvas.height)
    save_canvas_ctx.putImageData(data_high_res, 0, 0)
    let url = save_canvas.toDataURL("image/png")
    let a = document.createElement("a")
    a.href = url
    a.style.display = "none"
    a.download = "mandelbrot.png"
    document.body.appendChild(a)
    a.click()

    setTimeout(function () {
        document.body.removeChild(a)
        window.URL.revokeObjectURL(url)
    }, 0)
    // save_canvas.width = 0
    // save_canvas.height = 0
    draw()

})


precision_slider.addEventListener("change", event => {
    viewport.set_max_i(parseInt(precision_slider.value, 10))
    requestUpdate()
})

/* class Viewport {
    width: number
    height: number
    offsetX: number
    offsetY: number

    constructor(width: number, height: number, offsetX: number, offsetY: number) {
        this.width = width
        this.height = height
        this.offsetX = offsetX
        this.offsetY = offsetY
    }
    scaleX(x: number) {
        return (x / WIDTH) * this.width + this.offsetX
    }
    scaleY(y: number) {
        return (y / HEIGHT) * this.height + this.offsetY
    }
    zoom(multiply: number) {
        this.width = this.width * 1 / multiply
        this.height = this.height * 1 / multiply
        this.offsetX = this.offsetX * 1 / multiply
        this.offsetY = this.offsetY * 1 / multiply


    }

} */

const viewportDivider = HEIGHT / 3

let viewport = MandelbrotViewport.new(WIDTH / viewportDivider, HEIGHT / viewportDivider, -0.7, 0, WIDTH, HEIGHT)

precision_slider.value = viewport.get_max_i().toString()


input_x.value = viewport.get_x().toString()
input_y.value = viewport.get_y().toString()


let zoom = 1

canvas.addEventListener("wheel", (event) => {
    event.preventDefault()

    let delta = event.deltaY
    zoom = zoom - (delta / HEIGHT) * 4
    // console.log(zoom)
    // viewport.zoom(zoom)
    // draw()
    requestUpdate()
})

let startX = 0
let startY = 0

let mousedown = false

canvas.addEventListener("mousedown", event => {
    startX = event.offsetX
    startY = event.offsetY
    mousedown = true

})
canvas.addEventListener("mouseup", event => {
    mousedown = false
})
canvas.addEventListener("mouseleave", event => {
    mousedown = false
})


canvas.addEventListener("mousemove", event => {
    if (mousedown) {

        let deltaX = startX - event.offsetX
        let deltaY = startY - event.offsetY

        viewport.move_delta(deltaX, deltaY)
        startX = event.offsetX
        startY = event.offsetY
        input_x.value = viewport.get_x().toString()
        input_y.value = viewport.get_y().toString()
    }

    requestUpdate()
})

input_x.addEventListener("change", () => {
    viewport.set_x(parseFloat(input_x.value))
    requestUpdate()
})

input_y.addEventListener("change", () => {
    viewport.set_y(parseFloat(input_y.value))
    requestUpdate()
})

const zoom_in = () => {
    viewport.zoom(1.1)
    requestUpdate()
}
const zoom_out = () => {
    viewport.zoom(0.9)
    requestUpdate()
}


let zoom_interval_ms = 30

let zoom_direction = 0

const zoom_loop = () => {
    if (zoom_direction != 0) {

        viewport.zoom(1 + (zoom_direction / 10))
        requestUpdate()
    }
}
setInterval(zoom_loop, zoom_interval_ms)

zoom_in_button.addEventListener("mousedown", () => {
    zoom_direction = +1
})
zoom_in_button.addEventListener("mouseup", () => {
    zoom_direction = 0
})
zoom_in_button.addEventListener("mouseleave", () => {
    zoom_direction = 0
})
zoom_out_button.addEventListener("mousedown", () => {
    zoom_direction = -1
})
zoom_out_button.addEventListener("mouseup", () => {
    zoom_direction = 0
})
zoom_out_button.addEventListener("mouseleave", () => {
    zoom_direction = 0
})
let updateRequested = false

function requestUpdate() {
    if (!updateRequested) {
        updateRequested = true
        requestAnimationFrame(draw)

    }

}

function get_data(v: MandelbrotViewport) {
    let out = new Uint8ClampedArray(memory.buffer, v.get_array(), v.get_target_height() * v.get_target_width() * 4)
    return out

}


let values: Array<number> = []
let sum = 0

function draw() {
    let avant = performance.now()
    const data = new ImageData(get_data(viewport), WIDTH, HEIGHT)
    viewport.zoom(zoom)
    zoom = 1
    viewport.draw()
    sum = 0

    ctx.putImageData(data, 0, 0)
    updateRequested = false
    values.push(performance.now() - avant)
    if (values.length > 100) {
        values.shift()
    }
    values.forEach(value => {
        sum += value
    })
    perf_moy_span.innerText = (sum / values.length).toFixed(1)

    // requestAnimationFrame(draw)
}
requestAnimationFrame(draw)

